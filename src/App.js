import React, { Component } from 'react';
import Survey from './scenes/Survey/Survey';

class App extends Component {
  render() {
    return (
      <Survey />
    );
  }
}

export default App;
