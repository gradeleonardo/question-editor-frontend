export default class Config {
	static localUrl = 'http://localhost:3001';
	static url = 'https://question-editor-backend.herokuapp.com';

	static getPath() {
		// return Config.localUrl;
		return Config.url;
	}
}