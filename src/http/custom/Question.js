import BaseApi from '../Base';
import Config from '../Config';

class QuestionApi extends BaseApi {
	static plural = 'questions';
	constructor() {
		super(QuestionApi.plural);
	}

	/**
	 * @method createColumn
	 * @param {string} id - The question id
	 * @param {Object} columnObject - column with its properties.
	 * @param {Object} customHeaders - Custom Headers Information.
	 * @return {Promise} - created column
	 * @description
	 * Returns a promise that resolves with the created column
	 */
	createColumn(id, columnObject, customHeaders) {
		let _method = 'POST';
		let _url = `${Config.getPath()}/${this.getPlural()}/${id}/columns`;
		let _postBody = JSON.stringify(columnObject);
		let _customHeaders = { "Content-Type": "application/json", ...customHeaders }
		return this.request(_method, _url, _postBody, _customHeaders);
	}

	/**
	 * @method createRow
	 * @param {string} id - The question id
	 * @param {Object} rowObject - row with its properties.
	 * @param {Object} customHeaders - Custom Headers Information.
	 * @return {Promise} - created row
	 * @description
	 * Returns a promise that resolves with the created row
	 */
	createRow(id, rowObject, customHeaders) {
		let _method = 'POST';
		let _url = `${Config.getPath()}/${this.getPlural()}/${id}/rows`;
		let _postBody = JSON.stringify(rowObject);
		let _customHeaders = { "Content-Type": "application/json", ...customHeaders }
		return this.request(_method, _url, _postBody, _customHeaders);
	}

	getModelName() {
		return 'Question';
	}

	getPlural() {
		return this.plural;
	}
}

export default QuestionApi;