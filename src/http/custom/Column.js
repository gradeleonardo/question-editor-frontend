import BaseApi from '../Base';
import Config from '../Config';

class ColumnApi extends BaseApi {
	static plural = 'columns';
	constructor() {
		super(ColumnApi.plural);
	}

	/**
	 * @method uploadImg
	 * @param {string} columnId - The column id
	 * @param {Object} image - file.
	 * @param {Object} customHeaders - Custom Headers Information.
	 * @return {Promise} - uploaded img
	 * @description
	 * Returns a promise that resolves with the column img uploaded
	 */
	uploadImg(columnId, image, customHeaders) {
		const formData = new FormData();
		formData.append('img', image);
		let _method = 'POST';
		let _url = `${Config.getPath()}/${this.getPlural()}/${columnId}/img`;
		let _postBody = formData;
		return this.request(_method, _url, _postBody, null);
	}

	/**
	 * @method updateColumn
	 * @param {string} columnId - The column id
	 * @param {Object} column - object with properties to be updated.
	 * @param {Object} customHeaders - Custom Headers Information.
	 * @return {Promise} - updated column
	 * @description
	 * Returns a promise that resolves with the updated column
	 */
	updateColumn(columnId, column, customHeaders) {
		let _method = 'PUT';
		let _url = `${Config.getPath()}/${this.getPlural()}/${columnId}`;
		let _postBody = column;
		return this.request(_method, _url, _postBody, null);
	}

	getModelName() {
		return 'Column';
	}

	getPlural() {
		return this.plural;
	}
}

export default ColumnApi;