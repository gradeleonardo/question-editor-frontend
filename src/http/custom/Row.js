import BaseApi from '../Base';
import Config from '../Config';

class RowApi extends BaseApi {
	static plural = 'rows';
	constructor() {
		super(RowApi.plural);
	}

	/**
	 * @method uploadImg
	 * @param {string} rowId - The row id
	 * @param {Object} image - file.
	 * @param {Object} customHeaders - Custom Headers Information.
	 * @return {Promise} - uploaded img
	 * @description
	 * Returns a promise that resolves with the row img uploaded
	 */
	uploadImg(rowId, image, customHeaders) {
		const formData = new FormData();
		formData.append('img', image);
		let _method = 'POST';
		let _url = `${Config.getPath()}/${this.getPlural()}/${rowId}/img`;
		let _postBody = formData;
		return this.request(_method, _url, _postBody, null);
	}

	/**
	 * @method updateRow
	 * @param {string} rowId - The row id
	 * @param {Object} row - object with properties to be updated.
	 * @param {Object} customHeaders - Custom Headers Information.
	 * @return {Promise} - updated row
	 * @description
	 * Returns a promise that resolves with the updated row
	 */
	updateRow(rowId, row, customHeaders) {
		let _method = 'PUT';
		let _url = `${Config.getPath()}/${this.getPlural()}/${rowId}`;
		let _postBody = row;
		return this.request(_method, _url, _postBody, null);
	}

	getModelName() {
		return 'Row';
	}

	getPlural() {
		return this.plural;
	}
}

export default RowApi;