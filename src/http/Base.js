import Config from './Config';

export default class BaseApi {
	plural;
	constructor(plural) {
		this.plural = plural;
	}

	request(method, url, postBody, headers) {
		let _headers = {
			// 'Content-Type': 'application/json',
			...headers
		};

		return fetch(url, {
			method: method,
			body: postBody,
			headers: _headers
		})
			.then(response => {
				if (!response.ok) {
					throw Error(response.statusText);
				}
				if (response.statusText === 'No Content') {
					return response;
				} else {
					return response.json();
				}
			});
	}

	/**
	 * @method create
	 * @param {T} data Generic data type
	 * @return {Promise}
	 * @description
	 * Generic create method
	 */
	create(data, customHeaders) {
		let _method = 'POST';
		let _url = `${Config.getPath()}/${this.getPlural()}`;
		let _customHeaders = { "Content-Type": "application/json", ...customHeaders }
		return this.request(_method, _url, JSON.stringify(data), _customHeaders);
	}

	/**
	 * @method findById
	 * @param {string} id - Generic Object id
	 * @return {Promise} - Return Generic Object
	 * @description
	 * Generic findById method
	 */
	findById(id, customHeaders) {
		let _method = 'GET';
		let _url = `${Config.getPath()}/${this.getPlural()}/${id}`;
		return this.request(_method, _url, null, customHeaders);
	}

	/**
	 * @method find
	 * @param {Object} customHeaders - Custom headers object
	 * @return {Promise} - Return Generic Object
	 * @description
	 * Generic findById method
	 */
	find(customHeaders) {
		let _method = 'GET';
		let _url = `${Config.getPath()}/${this.getPlural()}/`;
		return this.request(_method, _url, null, customHeaders);
	}

	update(id, data, customHeaders) {
		let _method = 'PUT';
		let _url = `${Config.getPath()}/${this.getPlural()}/${id}`;
		let _customHeaders = { "Content-Type": "application/json", ...customHeaders }
		return this.request(_method, _url, JSON.stringify(data), _customHeaders);
	}

	delete(id) {
		let _method = 'DELETE';
		let _url = `${Config.getPath()}/${this.getPlural()}/${id}`;
		return this.request(_method, _url, null, null);
	}

	/**
   * @return {string}
   * @description
   * Abstract getModelName method
   */
	getModelName() {
		throw new Error('You have to implement the method getModelName!');
	}

	getPlural() {
		throw new Error('You have to implement the method getPlural!');
	}
}