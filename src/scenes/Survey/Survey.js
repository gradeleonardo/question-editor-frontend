import React, { Component } from 'react';
import QuestionList from '../../components/QuestionList/QuestionList';

class Survey extends Component {
	constructor(props) {
		super(props);
		this.state = {
			canEdit: true
		};
	}

	handleInputChange = e => {
		const target = e.target;
		const value = target.type === 'checkbox' ? target.checked : target.value;
		const name = target.name;
		this.setState({
			[name]: value
		});
	};

	render() {
		const { canEdit } = this.state;
		return (
			<div>
				<label style={{ fontSize: '2em' }}>
					<input
						style={{ marginRight: 5 }}
						name="canEdit"
						value="Edit"
						type="checkbox"
						checked={canEdit}
						onChange={this.handleInputChange} />
					Edit
				</label>
				<QuestionList canEdit={canEdit} />
			</div>
		);
	}
}

export default Survey;