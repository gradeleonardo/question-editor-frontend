import React from 'react';

const styles = {
	input: {
		display: 'none',
		width: 0,
		height: 0
	},
	label: {
		cursor: 'pointer',
		margin: 0
	},
	img: {
		width: 32,
		maxHeight: 32
	}
};

export default function ImgUpload(props) {
	let content = <i className="fas fa-image"></i>;
	if (props.imgUrl && props.imgUrl.length) {
		content = <img alt="Item" style={styles.img} src={props.imgUrl} />;
	}
	const handleFileChanged = e => {
		props.onImgSelected(e.target.files[0]);
	}
	return (
		<td>
			<label htmlFor={props.id} style={styles.label}>
				{content}
			</label>
			<input style={styles.input} id={props.id} type="file" onChange={handleFileChanged} />
		</td>
	);
}

