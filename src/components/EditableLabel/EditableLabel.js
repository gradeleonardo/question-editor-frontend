import React, { Component } from 'react';
import AutosizeInput from 'react-input-autosize';

class EditableLabel extends Component {
	constructor(props) {
		super(props);
		this.state = {
			value: props.value,
			editing: false
		}
	}

	handleLabelClicked = () => {
		this.setState(prevState => {
			return {
				...prevState,
				editing: !prevState.editing
			}
		});
	}

	handleInputLostFocus = e => {
		this.setState({
			editing: false
		});
		this.props.onChange(e.target.value);
	};

	handleInputChange = e => {
		this.setState({
			value: e.target.value
		});
	};

	handleKeyDown = e => {
		if (e.key === 'Enter') {
			this.handleInputLostFocus(e);
		}
	};

	render() {
		const { editing, value } = this.state;
		const { canEdit } = this.props;

		const input = (
			<AutosizeInput
				type="text"
				value={value}
				onChange={this.handleInputChange}
				onBlur={this.handleInputLostFocus}
				onKeyDown={this.handleKeyDown}
				autoFocus />);
		const label = <span style={canEdit ? { cursor: 'pointer' } : null} onClick={canEdit ? this.handleLabelClicked : null}>{value}</span>;
		return (
			<div>
				{editing && canEdit ? input : label}
			</div>
		)
	};
}

export default EditableLabel;