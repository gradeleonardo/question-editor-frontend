import React, { Component } from 'react';
import MatrixRadioQuestion from '../MatrixRadioQuestion/MatrixRadioQuestion';
import Button from '../../components/ui/Button/Button';
import ColumnApi from '../../http/custom/Column';
import RowApi from '../../http/custom/Row';
import QuestionApi from '../../http/custom/Question';

const apis = {
	question: new QuestionApi(),
	column: new ColumnApi(),
	row: new RowApi()
};

class QuestionList extends Component {
	constructor(props) {
		super(props);
		this.state = {
			questions: []
		};
	}

	componentWillMount() {
		apis.question
			.find(null)
			.then(questions => {
				this.setState({ questions });
			});
	}

	handleOnAddQuestion = () => {
		apis.question
			.create({ title: 'Question Title' }, null)
			.then(question => {
				this.setState(prevState => {
					return { ...prevState, questions: prevState.questions.concat(question) }
				})
			});
	}

	handleAnswer = ({ rowId, columnId, questionId }) => { };

	handleAddRow = questionId => {
		apis.question
			.createRow(questionId, { label: `row${this.state.questions.find(q => q._id === questionId).rows.length + 1}` }, null)
			.then(row => {
				this.setState(prevState => {
					const prevQuestionIndex = prevState.questions.findIndex(quest => quest._id === questionId);
					const prevQuestion = prevState.questions[prevQuestionIndex];
					const updatedRows = prevQuestion.rows.concat(row);
					const updatedQuestion = { ...prevQuestion, rows: updatedRows };
					const updatedQuestions = Object.assign([...prevState.questions], { [prevQuestionIndex]: updatedQuestion });

					return {
						...prevState,
						questions: updatedQuestions
					}
				});
			});
	};

	handleDeleteRow = ({ rowId, questionId }) => {
		apis.row
			.delete(rowId)
			.then(() => {
				this.setState(prevState => {
					const prevQuestionIndex = prevState.questions.findIndex(q => q._id === questionId);
					const prevQuestion = prevState.questions[prevQuestionIndex];
					const updatedRows = prevQuestion.rows.filter(r => r._id !== rowId);
					const updatedQuestion = { ...prevQuestion, rows: updatedRows };
					const updatedQuestions = Object.assign([...prevState.questions], { [prevQuestionIndex]: updatedQuestion });

					return {
						...prevState,
						questions: updatedQuestions
					}
				});
			});
	};

	handleAddColumn = questionId => {
		apis.question
			.createColumn(questionId, { label: `col${this.state.questions.find(q => q._id === questionId).columns.length + 1}` }, null)
			.then(column => {
				this.setState(prevState => {
					const prevQuestionIndex = prevState.questions.findIndex(quest => quest._id === questionId);
					const prevQuestion = prevState.questions[prevQuestionIndex];
					const updatedCols = prevQuestion.columns.concat(column);
					const updatedQuestion = { ...prevQuestion, columns: updatedCols };
					const updatedQuestions = Object.assign([...prevState.questions], { [prevQuestionIndex]: updatedQuestion });

					return {
						...prevState,
						questions: updatedQuestions
					}
				});
			});

	};

	handleDeleteColumn = ({ colId, questionId }) => {
		apis.column
			.delete(colId)
			.then(() => {
				this.setState(prevState => {
					const prevQuestionIndex = prevState.questions.findIndex(q => q._id === questionId);
					const prevQuestion = prevState.questions[prevQuestionIndex];
					const updatedColumns = prevQuestion.columns.filter(r => r._id !== colId);
					const updatedQuestion = { ...prevQuestion, columns: updatedColumns };
					const updatedQuestions = Object.assign([...prevState.questions], { [prevQuestionIndex]: updatedQuestion });

					return {
						...prevState,
						questions: updatedQuestions
					}
				});
			});

	};

	handleOnTitleChange = ({ title, questionId }) => {
		apis.question
			.update(questionId, { title }, null)
			.then(question => {
				this.setState(prevState => {
					const prevQuestionIdx = prevState.questions.findIndex(q => q._id === questionId);
					const prevQuestion = prevState.questions[prevQuestionIdx];
					const updatedQuestion = { ...prevQuestion, title: question.title };
					const updatedQuestions = Object.assign([...prevState.questions], { [prevQuestionIdx]: updatedQuestion });

					return {
						...prevState,
						questions: updatedQuestions
					}
				});
			});
	};

	handleColumnLabelChanged = ({ col, questionId }) => {
		apis.column
			.update(col._id, { label: col.label }, null)
			.then(column => {
				this.setState(prevState => {
					const prevQuestionIndex = prevState.questions.findIndex(quest => quest._id === questionId);
					const prevQuestion = prevState.questions[prevQuestionIndex];
					const prevColumnIndex = prevQuestion.columns.findIndex(c => c._id === col._id);
					const updatedColumns = Object.assign([...prevQuestion.columns], { [prevColumnIndex]: column });
					const updatedQuestion = { ...prevQuestion, columns: updatedColumns };
					const updatedQuestions = Object.assign([...prevState.questions], { [prevQuestionIndex]: updatedQuestion });

					return {
						...prevState,
						questions: updatedQuestions
					}
				});
			});
	};

	handleRowLabelChanged = ({ row, questionId }) => {
		apis.row
			.update(row._id, { label: row.label }, null)
			.then(row => {
				this.setState(prevState => {
					const prevQuestionIndex = prevState.questions.findIndex(quest => quest._id === questionId);
					const prevQuestion = prevState.questions[prevQuestionIndex];
					const prevRowIndex = prevQuestion.rows.findIndex(r => r._id === row._id);
					const updatedRows = Object.assign([...prevQuestion.rows], { [prevRowIndex]: row });
					const updatedQuestion = { ...prevQuestion, rows: updatedRows };
					const updatedQuestions = Object.assign([...prevState.questions], { [prevQuestionIndex]: updatedQuestion });

					return {
						...prevState,
						questions: updatedQuestions
					}
				});
			});
	};

	handleOnEditImg = ({ questionId, colId, rowId, img }) => {
		const model = colId ? 'column' : 'row';
		const modelPlural = model => model + 's';
		const id = colId ? colId : rowId;

		apis[model]
			.uploadImg(id, img)
			.then(updatedObj => {
				this.setState(prevState => {
					const prevQuestionIndex = prevState.questions.findIndex(quest => quest._id === questionId);
					const prevQuestion = prevState.questions[prevQuestionIndex];
					const prevObjIndex = prevQuestion[modelPlural(model)].findIndex(o => o._id === id);
					const updatedProperties = Object.assign([...prevQuestion[modelPlural(model)]], { [prevObjIndex]: updatedObj });
					const updatedQuestion = { ...prevQuestion, [modelPlural(model)]: updatedProperties };
					const updatedQuestions = Object.assign([...prevState.questions], { [prevQuestionIndex]: updatedQuestion });
					return {
						...prevState,
						questions: updatedQuestions
					}
				});
			})
			.catch(err => {
				console.log(err);
			});
	}

	render() {
		const { questions } = this.state;

		let content = this.props.canEdit ? <p>Please click on Add Question to add your first question.</p> : <p>Ops... There are no questions here :(</p>;
		if (questions.length) {
			content = questions.map(q => (
				<MatrixRadioQuestion
					key={q._id}
					question={q}
					canEdit={this.props.canEdit}
					onAddRow={this.handleAddRow}
					onDeleteRow={this.handleDeleteRow}
					onDeleteColumn={this.handleDeleteColumn}
					onAddColumn={this.handleAddColumn}
					onTitleChanged={this.handleOnTitleChange}
					onColumnLabelChanged={this.handleColumnLabelChanged}
					onRowLabelChanged={this.handleRowLabelChanged}
					onEditImg={this.handleOnEditImg}
					handleAnswer={this.handleAnswer} />
			));
		}
		const addButton = this.props.canEdit ? <Button text="Add Question" onClick={this.handleOnAddQuestion} /> : null;

		return (
			<div style={{ paddingBottom: 10 }}>
				{content}
				<div style={{ textAlign: 'center' }}>{addButton}</div>
			</div>
		);
	}
}

export default QuestionList;