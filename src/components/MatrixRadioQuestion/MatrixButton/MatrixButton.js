import React from 'react';

function MatrixButton(props) {
	let button;
	if (props.type === 'add') {
		button = <td className="add-button" onClick={props.onClick}><i className="fas fa-plus"></i></td>;
	} else {
		button = <td className="remove-button" onClick={props.onClick}><i className="fas fa-trash-alt"></i></td>
	}

	return button;
}

export default MatrixButton;