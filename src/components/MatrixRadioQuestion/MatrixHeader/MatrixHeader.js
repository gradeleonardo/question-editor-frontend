import React from 'react';
import EditableLabel from '../../EditableLabel/EditableLabel';
import ImgUpload from '../../ImgUpload/ImgUpload';
import MatrixButton from '../MatrixButton/MatrixButton'

function MatrixHeader(props) {
	if (props.columns) {
		const emptySpacesNumber = props.nRows ? (props.canEdit ? 3 : 2) : 1; // adjust empty th considering the label space for the row

		const emptySpaces = () => Array.from({ length: emptySpacesNumber }, (_, i) => (<th key={'emptyTh' + i} />));
		return (
			<thead>
				<DeleteRow
					canEdit={props.canEdit}
					emptySpacesGenerator={emptySpaces}
					columns={props.columns}
					onDeleteColumn={props.onDeleteColumn}
				/>
				<ImgsRow
					emptySpacesGenerator={emptySpaces}
					imgs={props.columns}
					canEdit={props.canEdit}
					onAddColumn={props.onAddColumn}
					onImgSelected={props.onEditImg} />
				<LabelsRow
					emptySpacesGenerator={emptySpaces}
					labels={props.columns}
					canEdit={props.canEdit}
					onColumnLabelChanged={props.onColumnLabelChanged}
				/>
			</thead>
		);
	} else {
		return null;
	}
}

function DeleteRow(props) {
	const deleteIconArray = props.columns.map(col => <MatrixButton type="del" key={col._id} onClick={() => props.onDeleteColumn(col._id)} />);
	if (!props.canEdit) {
		return null;
	}
	return (
		<tr>
			{props.emptySpacesGenerator()}
			{deleteIconArray}
		</tr>
	);

}

function ImgsRow(props) {
	const imgs = props.imgs.map((col, i) => (
		<ImgUpload
			id={col._id}
			key={'col-img-' + col._id}
			imgUrl={col.imgUrl}
			onImgSelected={img => props.onImgSelected({ colId: col._id, img })}
		/>)
	);
	const button = props.canEdit ? <MatrixButton type="add" onClick={props.onAddColumn} /> : null;

	return (
		<tr>
			{props.emptySpacesGenerator()}
			{imgs}
			{button}
		</tr>
	);
}

function LabelsRow(props) {
	const labels = props.labels.map(col => {
		const handleColumnLabelChanged = newColLabel => props.onColumnLabelChanged({ ...col, label: newColLabel });
		return (
			<th key={'th-' + col._id}>
				<EditableLabel
					value={col.label}
					onChange={handleColumnLabelChanged}
					canEdit={props.canEdit} />
			</th>
		);
	});

	if (props.labels.length) {
		return (
			<tr>
				{props.emptySpacesGenerator()}
				{labels}
			</tr>
		);
	} else {
		return null;
	}
}

export default MatrixHeader;