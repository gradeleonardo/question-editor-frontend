import React from 'react';
import Card from '../../ui/card/card';

import { Col, Row } from 'reactstrap';

const styles = {
	wrapper: {
		marginBottom: 5,
		padding: 5
	},
	title: {
		fontSize: '1.4rem',
		textTransform: 'uppercase',
		fontWeight: 'bold'
	}
};

function SummaryView(props) {
	const cards = props.summaryViewList.map(i => {
		return (
			<div style={styles.wrapper}>
				<div style={styles.title}>
					{i.title}
				</div>
				<Row>
					{i.cards.map(c => (
						<Col>
							<Card
								title={c.title}
								value={c.value}
							/>
						</Col>
					))}
				</Row>
			</div>
		);
	});

	return (
		<div>
			<div className="title">Question Summary View</div>
			<div className="summary-title">Summary</div>
			<div className="summary-content">
				{cards}
			</div>
		</div>
	);
}
export default SummaryView;