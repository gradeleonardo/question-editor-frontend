import React, { Component } from 'react';
import MatrixRow from '../MatrixRow/MatrixRow';
import MatrixButton from '../MatrixButton/MatrixButton'

class MatrixBody extends Component {
	render() {
		const tableRows = this.props.rows.map((row, i, arr) => {
			const handleRowLabelChanged = newRowLabel => this.props.onRowLabelChanged({ ...row, label: newRowLabel });
			return (
				<MatrixRow
					key={row._id}
					row={row}
					columns={this.props.columns}
					onSelectAnswer={this.props.onSelectAnswer}
					onRowLabelChanged={handleRowLabelChanged}
					onEditImg={this.props.onEditImg}
					canEdit={this.props.canEdit}
					onDeleteRow={this.props.onDeleteRow}
					isLast={(i === arr.length - 1)}
				/>
			);
		});
		const tableRowAddButton = this.props.canEdit && <tr><MatrixButton type="add" onClick={this.props.onAddRow} /></tr>;

		return (
			<tbody>
				{tableRows}
				{tableRowAddButton}
			</tbody>
		);
	}
}

export default MatrixBody;