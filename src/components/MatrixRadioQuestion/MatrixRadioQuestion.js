import React, { Component } from 'react';
import MatrixHeader from './MatrixHeader/MatrixHeader';
import MatrixBody from './MatrixBody/MatrixBody';
import SummaryView from './SummaryView/SummaryView';
import './MatrixRadioQuestion.css';
import { Row, Col } from 'reactstrap';
import EditableLabel from '../EditableLabel/EditableLabel';

class MatrixRadioQuestion extends Component {
	constructor(props) {
		super(props);
		this.state = {
			title: {
				value: ''
			}
		};
	}

	// Pass the handlers up to the parent
	handleAnswer = answer => this.props.handleAnswer({ ...answer, questionId: this.props.question._id });

	handleAddRow = () => this.props.onAddRow(this.props.question._id);

	handleAddColumn = () => this.props.onAddColumn(this.props.question._id);

	handleTitleChanged = title => this.props.onTitleChanged({ questionId: this.props.question._id, title });

	handleColumnLabelChanged = col => this.props.onColumnLabelChanged({ questionId: this.props.question._id, col });

	handleRowLabelChanged = row => this.props.onRowLabelChanged({ questionId: this.props.question._id, row });

	handleEditImg = ({ rowId, colId, img }) => { this.props.onEditImg({ questionId: this.props.question._id, colId, rowId, img }) };

	handleDeleteColumn = colId => this.props.onDeleteColumn({ questionId: this.props.question._id, colId });

	handleDeleteRow = rowId => this.props.onDeleteRow({ questionId: this.props.question._id, rowId });

	getLabelStats = (labels, option) => {
		return labels.reduce((prev, curr) => {
			if (option === 'longest') {
				return (curr.label.length > prev) ? curr.label.length : prev;
			} else if (option === 'shortest') {
				return (curr.label.length < prev || prev === 0) ? curr.label.length : prev;
			} else {
				return prev;
			}
		}, 0);
	};

	getUploadedImgsAmount = models => {
		const question = this.props.question;
		return models.reduce((prev, curr) => {
			return prev + (question[curr].filter(x => x.imgUrl)).length;
		}, 0);
	};

	generateSummaryViewList = question => {
		return ['rows', 'columns', 'images']
			.map(i => {
				if (i === 'images') {
					return {
						title: i,
						cards: [
							{
								title: 'total',
								value: this.getUploadedImgsAmount(['rows', 'columns'])
							},
							{
								title: 'rows',
								value: this.getUploadedImgsAmount(['rows'])
							},
							{
								title: 'columns',
								value: this.getUploadedImgsAmount(['columns'])
							}
						]
					}
				} else {
					return {
						title: i,
						cards: [
							{
								title: 'number',
								value: question[i] ? question[i].length : 0
							},
							{
								title: 'shortest label',
								value: this.getLabelStats(question[i], 'shortest')
							},
							{
								title: 'longest label',
								value: this.getLabelStats(question[i], 'longest')
							}
						]
					};
				}
			});
	};

	render() {
		const { question, canEdit } = this.props;

		return (
			<div className="question">
				<Row>
					<Col md={6}>
						<div className="edition-view">
							<div className="title">Question Edition View</div>
							<div className="question-title">
								<EditableLabel
									value={question.title}
									onChange={this.handleTitleChanged}
									canEdit={canEdit} />
							</div>
							<div className="question-matrix">
								<table>
									<MatrixHeader
										columns={question.columns}
										nRows={question.rows.length}
										onAddColumn={this.handleAddColumn}
										onDeleteColumn={this.handleDeleteColumn}
										onColumnLabelChanged={this.handleColumnLabelChanged}
										onEditImg={this.handleEditImg}
										canEdit={canEdit} />
									<MatrixBody
										rows={question.rows}
										columns={question.columns}
										onSelectAnswer={this.handleAnswer}
										onAddRow={this.handleAddRow}
										onDeleteRow={this.handleDeleteRow}
										onRowLabelChanged={this.handleRowLabelChanged}
										onEditImg={this.handleEditImg}
										canEdit={canEdit} />
								</table>
							</div>
						</div>
					</Col>
					<Col md={6}>
						<div className="summary-view">
							<SummaryView
								summaryViewList={this.generateSummaryViewList(question)}
							/>
						</div>
					</Col>
				</Row>
			</div>
		);
	}
}

export default MatrixRadioQuestion;
