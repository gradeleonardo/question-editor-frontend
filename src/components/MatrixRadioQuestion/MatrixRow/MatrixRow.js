import React, { Component } from 'react';
import EditableLabel from '../../EditableLabel/EditableLabel';
import ImgUpload from '../../ImgUpload/ImgUpload';
import MatrixButton from '../MatrixButton/MatrixButton';

class MatrixRow extends Component {
	state = {
		checkedColumn: null
	}

	handleInputRadioChange = e => {
		let colId = e.target.value;
		this.setState(prevState => {
			return {
				...prevState,
				checkedColumn: colId
			}
		});
		this.props.onSelectAnswer({
			rowId: this.props.row._id,
			columnId: colId
		});
	};

	generateTDs = () => {
		const { row, columns } = this.props;
		return columns.map(col => (
			<td key={row._id + col._id}>
				<label>
					<input
						type="radio"
						value={col._id}
						checked={this.state.checkedColumn === col._id}
						onChange={this.handleInputRadioChange} />
				</label>
			</td>
		));
	};

	render() {
		const handleOnEditImg = img => this.props.onEditImg({ rowId: this.props.row._id, img });
		const deleteButton = this.props.canEdit ? <MatrixButton type="del" onClick={() => this.props.onDeleteRow(this.props.row._id)} /> : null;
		return (
			<tr>
				{deleteButton}
				<ImgUpload
					id={this.props.row._id}
					key={'row-img-' + this.props.row._id}
					imgUrl={this.props.row.imgUrl}
					onImgSelected={handleOnEditImg}
				/>
				<td>
					<EditableLabel
						value={this.props.row.label}
						onChange={this.props.onRowLabelChanged}
						canEdit={this.props.canEdit} />
				</td>
				{this.generateTDs()}
			</tr>
		);
	}
}

export default MatrixRow;