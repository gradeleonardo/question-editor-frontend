import React from 'react';
import './Button.css';
function Button(props) {
	return (
		<a onClick={props.onClick} className="btn btn--green">{props.text}</a>
	);
}

export default Button;