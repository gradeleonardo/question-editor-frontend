import React from 'react';

const styles = {
	wrapper: {
		padding: 5,
		marginBottom: 5,
		textAlign: 'center',
		backgroundColor: '#f6f6f6'
	},
	title: {
		textTransform: 'uppercase'
	},
	content: {
		textAlign: 'center',
		fontSize: '3rem'
	}
};

function Card(props) {
	return (
		<div style={styles.wrapper}>
			<div style={styles.title}>{props.title}</div>
			<div style={styles.content}>{props.value}</div>
		</div>
	);
}

export default Card;