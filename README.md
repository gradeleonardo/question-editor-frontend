# question editor frontend

## How to Run the Task
`npm i && npm start`

If you would like to setup the frontend to reach a local version of [backend](https://gitlab.com/gradeleonardo/question-editor-backend), edit `/http/Config.js` by commenting `return Config.url;` and uncommenting `return Config.localUrl;`.

## Task Description

- Ability to select images from the hard drive for every row and column
- Ability to set labels for every row and column
- Ability to add new rows and columns
- Ability to remove rows and columns

- A statistics pane on the right:
    - Amount of rows created
    - Amount of columns created
    - Amount of images uploaded
    - The string length of the longest label
    - The string length of the shortest label
- Optional: Animations (for example when adding a row or column)